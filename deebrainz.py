#!/usr/bin/env python3

import os
import tempfile
import time
from os.path import exists
from types import SimpleNamespace

import click
import requests
from rich.console import Console
from rich.prompt import Prompt
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import Select, WebDriverWait

CONSOLE = Console(stderr=True)
CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])

COOKIE_FILE_PATH = "./cookie"
COOKIE_NAME = "musicbrainz_server_session"

TIMEOUT = 30
TIMEOUT_LONG = TIMEOUT * 10

WARNING = ":warning: Warning"


@click.command(context_settings=CONTEXT_SETTINGS)
@click.option(
    "-r", "--real", help="Use the real server instead of the test one.", is_flag=True
)
@click.argument("album_id")
def main(album_id, real):
    cookie = None
    # Check if we have a session cookie saved, and if we do, use it.
    if exists(COOKIE_FILE_PATH):
        with open(COOKIE_FILE_PATH) as cookie_file:
            cookie = cookie_file.read()
    # Start the web driver.
    with webdriver.Firefox() as driver:
        # Depending on the user's choice, we'll use the test server or the real one.
        test_server = not real
        links = Links(test_server)
        if cookie:
            driver.get(links.home_page)
            driver.add_cookie(
                {
                    "name": COOKIE_NAME,
                    "value": cookie,
                }
            )
            # Check if the session cookie is valid.
            driver.get(links.add_release_page)
            try:
                driver.find_element_by_id("id-username")
                # If an exception isn't raised, it means the session cookie isn't valid.
                log_in(driver, links)
            except NoSuchElementException:
                # The session cookie is valid.
                pass
        else:
            log_in(driver, links)
        # Get the album's information.
        album = requests.get(f"https://api.deezer.com/album/{album_id}")

        def hook(dictionary):
            return SimpleNamespace(**dictionary)

        album = album.json(object_hook=hook)
        driver.get(links.add_release_page)
        fill_release_information(driver, album)
        fill_tracklist(driver, album.tracks.data)
        fill_edit_notes(driver, album.link)
        CONSOLE.print(
            "Please check that the entered information is correct and fill the remaining fields where possible."
        )
        CONSOLE.input(
            "Once you have entered the edit and been redirected, press enter to submit the cover art..."
        )
        add_cover_art(driver, album.cover_xl, album.link)
        CONSOLE.input(
            "Once you have entered the edit and the cover art has finished uploading, press enter to exit..."
        )


class Links:
    def __init__(self, test_server):
        self.test_server = test_server

    def replacer(function):
        def decorator(self):
            link = function(self)
            return link if self.test_server else link.replace("test.", "")

        return decorator

    @property
    @replacer
    def add_release_page(self):
        return "https://test.musicbrainz.org/release/add"

    @property
    @replacer
    def home_page(self):
        return "https://test.musicbrainz.org"

    @property
    @replacer
    def log_in_page(self):
        return "https://test.musicbrainz.org/login"

    @property
    @replacer
    def user_page(self):
        return "https://test.musicbrainz.org/user"


########################################
#   _    _ _   _ _ _ _   _             #
#  | |  | | | (_) (_) | (_)            #
#  | |  | | |_ _| |_| |_ _  ___  ___   #
#  | |  | | __| | | | __| |/ _ \/ __|  #
#  | |__| | |_| | | | |_| |  __/\__ \  #
#   \____/ \__|_|_|_|\__|_|\___||___/  #
#                                      #
########################################


def click_element(driver, element_id=None, xpath=None):
    element = get_element(driver, element_id, xpath)
    element.click()


def get_element(driver, element_id=None, xpath=None):
    element = None
    if element_id:
        element = driver.find_element_by_id(element_id)
    elif xpath:
        element = driver.find_element_by_xpath(xpath)
    return element


def select_dropdown_option(driver, option_text, dropdown_id=None, xpath=None):
    dropdown = get_element(driver, dropdown_id, xpath)
    dropdown = Select(dropdown)
    dropdown.select_by_visible_text(option_text)


def send_keys_to_element(driver, keys, element_id=None, xpath=None, click=True):
    element = get_element(driver, element_id, xpath)
    if click:
        element.click()
    element.send_keys(keys)


###################################
#   _                   _         #
#  | |                 (_)        #
#  | |     ___   __ _   _ _ __    #
#  | |    / _ \ / _` | | | '_ \   #
#  | |___| (_) | (_| | | | | | |  #
#  |______\___/ \__, | |_|_| |_|  #
#                __/ |            #
#               |___/             #
#                                 #
###################################


def log_in(driver, links):
    driver.get(links.log_in_page)
    wait = WebDriverWait(driver, TIMEOUT_LONG)
    user_page_loaded = expected_conditions.url_contains(links.user_page)
    wait.until(user_page_loaded)
    cookie = driver.get_cookie(COOKIE_NAME)
    # Save the session cookie.
    with open(COOKIE_FILE_PATH, "w") as cookie_file:
        cookie = SimpleNamespace(**cookie)
        cookie_file.write(cookie.value)


##################################################################################################
#   _____      _                       _        __                           _   _               #
#  |  __ \    | |                     (_)      / _|                         | | (_)              #
#  | |__) |___| | ___  __ _ ___  ___   _ _ __ | |_ ___  _ __ _ __ ___   __ _| |_ _  ___  _ __    #
#  |  _  // _ \ |/ _ \/ _` / __|/ _ \ | | '_ \|  _/ _ \| '__| '_ ` _ \ / _` | __| |/ _ \| '_ \   #
#  | | \ \  __/ |  __/ (_| \__ \  __/ | | | | | || (_) | |  | | | | | | (_| | |_| | (_) | | | |  #
#  |_|  \_\___|_|\___|\__,_|___/\___| |_|_| |_|_| \___/|_|  |_| |_| |_|\__,_|\__|_|\___/|_| |_|  #
#                                                                                                #
##################################################################################################


ARTIST_DROPDOWN_ID = "ui-id-6"
BARCODE_FIELD_ID = "barcode"
COUNTRY_DROPDOWN_ID = "country-0"
PACKAGING_DROPDOWN_ID = "packaging"
PRIMARY_TYPE_DROPDOWN_ID = "primary-type"
RELEASE_GROUP_DROPDOWN_ID = "ui-id-7"
RELEASE_GROUP_FIELD_ID = "release-group"
STATUS_DROPDOWN_ID = "status"
TITLE_FIELD_ID = "name"

ADD_LABEL_BUTTON_XPATH = "//button[@title='Add Label']"
ARTIST_FIELD_XPATH = "//span[@class='artist autocomplete']/input"
EXTERNAL_LINK_FIELD_XPATH = "//table[@id='external-links-editor']/tbody/tr/td[2]/input"


class MissingArtistError(Exception):
    pass


class MissingReleaseGroupError(Exception):
    pass


def fill_release_information(driver, album):
    # Prepare needed information.
    artist = album.artist
    date = time.strptime(album.release_date, "%Y-%m-%d")
    primary_type = None
    if album.record_type == "ep":
        primary_type = album.record_type.upper()
    else:
        primary_type = album.record_type.capitalize()
    title = album.title
    release_group = title
    # Start filling the fields.
    fill_title_field(driver, title)
    try:
        fill_artist_field(driver, artist.name)
    except MissingArtistError:
        CONSOLE.print(
            f"{WARNING}: Artist ({artist.name}) could not be found, artist field cleared."
        )
        CONSOLE.input("Press enter to continue...")
    try:
        fill_release_group_field(driver, release_group, primary_type, artist.name)
    except MissingReleaseGroupError:
        CONSOLE.print(
            f"{WARNING}: Release group ({release_group} - {primary_type} by {artist.name}) could not be found, release group field cleared."
        )
        fill_primary_type_field(driver, primary_type)
    fill_status_field(driver, "Official")
    fill_date_field(driver, date)
    fill_country_field(driver, "[Worldwide]")
    fill_label_field(driver, album.label)
    fill_barcode_field(driver, album.upc)
    fill_packaging_field(driver, "None")
    fill_external_link_field(driver, album.link)


def fill_title_field(driver, title):
    send_keys_to_element(driver, title, element_id=TITLE_FIELD_ID)


def fill_artist_field(driver, artist):
    artist_field = driver.find_element_by_xpath(ARTIST_FIELD_XPATH)
    artist_field.send_keys(artist)
    artist_dropdown = driver.find_element_by_id(ARTIST_DROPDOWN_ID)
    # Wait for the dropdown to load.
    wait = WebDriverWait(driver, TIMEOUT)
    artist_dropdown_is_visible = expected_conditions.visibility_of(artist_dropdown)
    try:
        wait.until(artist_dropdown_is_visible)
        artist_option = driver.find_element_by_xpath(
            f"//ul[@id='{ARTIST_DROPDOWN_ID}']/li/a[contains(., '{artist}')]"
        )
        artist_option.click()
    except NoSuchElementException:
        artist_field.clear()
        raise MissingArtistError


def fill_release_group_field(driver, release_group, primary_type, artist):
    release_group_field = driver.find_element_by_id(RELEASE_GROUP_FIELD_ID)
    # The release group field gets automatically filled when typing the title, so we need to clear it.
    release_group_field.clear()
    release_group_field.send_keys(release_group)
    release_group_dropdown = driver.find_element_by_id(RELEASE_GROUP_DROPDOWN_ID)
    # Wait for the dropdown to load.
    wait = WebDriverWait(driver, TIMEOUT)
    release_group_dropdown_is_visible = expected_conditions.visibility_of(
        release_group_dropdown
    )
    wait.until(release_group_dropdown_is_visible)
    try:
        release_group_options = driver.find_elements_by_xpath(
            f"//ul[@id='{RELEASE_GROUP_DROPDOWN_ID}']/li/a[contains(., '{release_group}')]"
        )
        for release_group_option in release_group_options:
            if f"{primary_type} by {artist}" in release_group_option.text:
                release_group_option.click()
                break
        else:
            raise NoSuchElementException
    except NoSuchElementException:
        release_group_field.clear()
        raise MissingReleaseGroupError


def fill_primary_type_field(driver, primary_type):
    select_dropdown_option(driver, primary_type, dropdown_id=PRIMARY_TYPE_DROPDOWN_ID)


def fill_status_field(driver, status):
    select_dropdown_option(driver, status, dropdown_id=STATUS_DROPDOWN_ID)


def fill_date_field(driver, date):
    year_field = driver.find_element_by_class_name("partial-date-year")
    year_field.send_keys(date.tm_year)
    month_field = driver.find_element_by_class_name("partial-date-month")
    month_field.send_keys(date.tm_mon)
    day_field = driver.find_element_by_class_name("partial-date-day")
    day_field.send_keys(date.tm_mday)


def fill_country_field(driver, country):
    select_dropdown_option(driver, country, dropdown_id=COUNTRY_DROPDOWN_ID)


def fill_label_field(driver, label):
    # This isn't the most robust way to check for multiple labels, but for now it will do.
    labels = label.split(" / ") if " / " in label else [label]
    for label_index, label in enumerate(labels):
        if label_index > 0:
            click_element(driver, xpath=ADD_LABEL_BUTTON_XPATH)
        label_id = f"label-{label_index}"
        label_field = driver.find_element_by_id(label_id)
        label_field.send_keys(label)
        label_dropdown_xpath = f"//ul[@data-input-id='{label_id}']"
        label_dropdown = driver.find_element_by_xpath(label_dropdown_xpath)
        # Wait for the dropdown to load.
        wait = WebDriverWait(driver, TIMEOUT)
        label_dropdown_is_visible = expected_conditions.visibility_of(label_dropdown)
        wait.until(label_dropdown_is_visible)
        try:
            label_options = driver.find_elements_by_xpath(
                f"{label_dropdown_xpath}/li/a"
            )
            for label_option in label_options:
                if label in label_option.text:
                    label_option.click()
                    break
            else:
                raise NoSuchElementException
        except NoSuchElementException:
            CONSOLE.print(
                f"{WARNING}: Label ({label}) could not be found, clearing label field."
            )
            label_field.clear()


def fill_barcode_field(driver, barcode):
    send_keys_to_element(driver, barcode, element_id=BARCODE_FIELD_ID)


def fill_packaging_field(driver, packaging):
    select_dropdown_option(driver, packaging, dropdown_id=PACKAGING_DROPDOWN_ID)


def fill_external_link_field(driver, link):
    send_keys_to_element(driver, link, xpath=EXTERNAL_LINK_FIELD_XPATH)


############################################
#   _______             _    _ _     _     #
#  |__   __|           | |  | (_)   | |    #
#     | |_ __ __ _  ___| | _| |_ ___| |_   #
#     | | '__/ _` |/ __| |/ / | / __| __|  #
#     | | | | (_| | (__|   <| | \__ \ |_   #
#     |_|_|  \__,_|\___|_|\_\_|_|___/\__|  #
#                                          #
############################################


TRACKLIST_TAB_ID = "ui-id-3"

ADD_TRACK_BUTTON_XPATH = "//button[@title='Add track(s)']"
FORMAT_DROPDOWN_XPATH = "//td[@class='format']/select"
TRACK_ARTIST_DROPDOWN_XPATH = "//body/ul[last()]"
TRACK_XPATH = "//tr[@class='track']"
TRACK_ARTIST_FIELD_XPATH = f"{TRACK_XPATH}//table[@class='artist-credit-editor']//input[@class='name ui-autocomplete-input']"
TRACK_LENGTH_FIELD_XPATH = f"{TRACK_XPATH}//input[@class='track-length']"
TRACK_TITLE_FIELD_XPATH = f"{TRACK_XPATH}//input[@class='track-name']"
TRACK_PARSER_CLOSE_BUTTON_XPATH = "//div[last()]/div/button[@title='close']"


def fill_tracklist(driver, tracks):
    click_element(driver, element_id=TRACKLIST_TAB_ID)
    click_element(driver, xpath=TRACK_PARSER_CLOSE_BUTTON_XPATH)
    fill_format_field(driver, "Digital Media")
    add_track_button = driver.find_element_by_xpath(ADD_TRACK_BUTTON_XPATH)
    for track_index, track in enumerate(tracks):
        add_track_button.click()
        fill_track_title_field(driver, track_index, track.title)
        # fill_track_artist_field(driver, track_index, track.artist.name)
        fill_track_length_field(driver, track_index, track.duration)


def fill_format_field(driver, tracklist_format):
    select_dropdown_option(driver, tracklist_format, xpath=FORMAT_DROPDOWN_XPATH)


def fill_track_title_field(driver, track_index, title):
    track_title_fields = driver.find_elements_by_xpath(TRACK_TITLE_FIELD_XPATH)
    track_title_fields[track_index].send_keys(title)


def fill_track_artist_field(driver, track_index, artist):
    track_artist_fields = driver.find_elements_by_xpath(TRACK_ARTIST_FIELD_XPATH)
    track_artist_field = track_artist_fields[track_index]
    track_artist_field.send_keys(artist)
    track_artist_dropdowns = driver.find_elements_by_xpath(TRACK_ARTIST_DROPDOWN_XPATH)
    track_artist_dropdown = track_artist_dropdowns[track_index]
    # Wait for the dropdown to load.
    wait = WebDriverWait(driver, TIMEOUT)
    track_artist_dropdown_is_visible = expected_conditions.visibility_of(
        track_artist_dropdown
    )
    wait.until(track_artist_dropdown_is_visible)
    try:
        click_element(
            driver, xpath=f"{TRACK_ARTIST_DROPDOWN_XPATH}/li/a[contains(., '{artist}')]"
        )
    except NoSuchElementException:
        CONSOLE.print(
            f"{WARNING}: Track artist ({artist}) could not be found, clearing track artist field."
        )
        track_artist_field.clear()


def fill_track_length_field(driver, track_index, length):
    track_length_fields = driver.find_elements_by_xpath(TRACK_LENGTH_FIELD_XPATH)
    length = time.gmtime(length)
    length = time.strftime("%M:%S", length)
    track_length_fields[track_index].send_keys(length)


####################################################
#   ______    _ _ _                 _              #
#  |  ____|  | (_) |               | |             #
#  | |__   __| |_| |_   _ __   ___ | |_ ___  ___   #
#  |  __| / _` | | __| | '_ \ / _ \| __/ _ \/ __|  #
#  | |___| (_| | | |_  | | | | (_) | ||  __/\__ \  #
#  |______\__,_|_|\__| |_| |_|\___/ \__\___||___/  #
#                                                  #
####################################################


RELEASE_EDIT_NOTE_FIELD_ID = "edit-note-text"
RELEASE_EDIT_NOTES_TAB_ID = "ui-id-5"


def fill_edit_notes(driver, album_link):
    click_element(driver, element_id=RELEASE_EDIT_NOTES_TAB_ID)
    send_keys_to_element(driver, f"{album_link}", element_id=RELEASE_EDIT_NOTE_FIELD_ID)


##################################################
#    _____                                 _     #
#   / ____|                               | |    #
#  | |     _____   _____ _ __    __ _ _ __| |_   #
#  | |    / _ \ \ / / _ \ '__|  / _` | '__| __|  #
#  | |___| (_) \ V /  __/ |    | (_| | |  | |_   #
#   \_____\___/ \_/ \___|_|     \__,_|_|   \__|  #
#                                                #
##################################################


COVER_ART_EDIT_NOTE_FIELD_ID = "id-add-cover-art.edit_note"

ADD_COVER_ART_BUTTON_XPATH = "//a/bdi[contains(., 'Add Cover Art')]"
ADD_COVER_ART_FIELD_XPATH = "//input[@class='add-files']"
COVER_ART_TAB_XPATH = "//a/bdi[contains(., 'Cover Art')]"
COVER_ART_TYPE_FRONT_CHECKBOX_XPATH = "//input[@value='1']"


def add_cover_art(driver, cover_art, album_link):
    click_element(driver, xpath=COVER_ART_TAB_XPATH)
    wait = WebDriverWait(driver, TIMEOUT)
    add_cover_art_button_is_visible = expected_conditions.visibility_of_element_located(
        (By.XPATH, ADD_COVER_ART_BUTTON_XPATH)
    )
    wait.until(add_cover_art_button_is_visible)
    click_element(driver, xpath=ADD_COVER_ART_BUTTON_XPATH)
    # Get the cover art's extension from its link.
    extension = os.path.splitext(cover_art)[1]
    # Download the cover art.
    with open(f"./cover{extension}", "wb") as cover_art_file:
        response = requests.get(cover_art)
        cover_art_file.write(response.content)
        # Upload the cover art.
        cover_art_path = os.path.abspath(cover_art_file.name)
        send_keys_to_element(
            driver, cover_art_path, xpath=ADD_COVER_ART_FIELD_XPATH, click=False
        )
    click_element(driver, xpath=COVER_ART_TYPE_FRONT_CHECKBOX_XPATH)
    # Type in the edit note where we got the image from.
    send_keys_to_element(
        driver, f"{album_link}", element_id=COVER_ART_EDIT_NOTE_FIELD_ID
    )


if __name__ == "__main__":
    # pylint: disable=no-value-for-parameter
    main()
