<div align="center">
  <img width="128" height="128" src="./deebrainz.png">
  <h1 align="center">deebrainz</h1>
  <p>A metadata bridge from <a href="https://www.deezer.com/">Deezer</a> to <a href="https://musicbrainz.org/">MusicBrainz</a></p>
</div>

## Notice

If you actually want to contribute to MusicBrainz, look at [a-tisket](https://atisket.pulsewidth.org.uk/) instead of deebrainz.

## Requirements

For deebrainz to work, you'll need [geckodriver](https://github.com/mozilla/geckodriver) and [Python 3](https://www.python.org/) installed.

With [scoop](https://github.com/lukesampson/scoop), you can install them with the following command:

```
scoop install geckodriver python
```

## Instructions

To import an album's metadata from Deezer, you'll need to know its ID.

You can get it from the album's URL (it's the last part).

E.g. the ID of this album `https://www.deezer.com/us/album/57654392` is `57654392`.

Once you have the album ID, you can feed it to deebrainz, like so:

```
python3 ./deebrainz.py 57654392
```

### Log in

On the first run, deebrainz will require you to log in into MusicBrainz.

This won't be needed every time you want add an album, since deebrainz will save the session cookie and use that until it's no longer valid.

### Servers

If the flag `--real` isn't passed, deebrainz will use MusicBrainz's test server, which is useful to test deebrainz's behavior without having to worry about messing stuff up.

To add an edit to the real server, you can use the following command:

```
python3 ./deebrainz.py --real ALBUM_ID
```

## Usage

```
$ python3 ./deebrainz.py --help
Usage: deebrainz.py [OPTIONS] ALBUM_ID

Options:
  -r, --real  Use the real server instead of the test one.
  -h, --help  Show this message and exit.
```
